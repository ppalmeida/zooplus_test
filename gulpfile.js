var gulp 		= require('gulp');
var uglify 		= require('gulp-uglify');
var plumber 	= require('gulp-plumber');
var minifyCss 	= require('gulp-minify-css');
var watch 		= require('gulp-watch');
var sass 		= require('gulp-sass');
var notify 		= require("gulp-notify");
var i 			= 0; // Just a compile counter to help visually in Terminal

function outputError(error) {
	console.error("Compiling error!", new Date());
	console.error(error);
	console.error('--x--');
}

function compileSass() {
	gulp.src('./src/sass/*.scss')
		.pipe(
				plumber({
					errorHandler: notify.onError(function(error) {
						outputError(error);
						return "Sass Error! " + error.message;
					})
				})
		)
		.pipe(sass())
		.pipe(gulp.dest('./src/css'));

	gulp.src('./src/css/*.css')
		.pipe(minifyCss())
		.pipe(gulp.dest('./www/css/'));

	console.log("Sass Compiled", i++);
}

function minifyJS() {
	return gulp.src('./src/js/**/*.js')
		.pipe(ugilify())
		.pipe(gulp.dest('./www/js/'))
}

function watchAll() {
	watch('./src/sass/**/*.scss', compileSass);
	watch('./src/js/**/*.js', minifyJS);
}


gulp.task("default", watchAll);

